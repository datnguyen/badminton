class BabiesController < ApplicationController
  before_filter :accessible?, :except => [:wheres_your_key]

  def wheres_your_key
    @invalid_access_code = false
    if request.post?
      access_code = params[:access_code]
      if access_code.blank?
        @invalid_access_code = true

      else
        @baby = Baby.where(access_code: access_code).first
        if @baby.blank?
          @invalid_access_code = true

        else
          puts @baby.inspect
          session[:current_user] = @baby
          redirect_to :action => :index
        end
      end
    end
  end

  def get_me_out
    reset_session
    redirect_to wheres_your_key_babies_url
  end

  # GET /babies
  # GET /babies.json
  def index
    @teams = {}

    babies = Baby.where.not(:team_id => nil)
    babies.each do |baby|
      @teams[baby.team_id] = [] if !@teams.include?(baby.team_id)
      @teams[baby.team_id] << baby
    end
  end

  def select_partner
    if request.post?
      count = random_animal = Baby.where(:gender => @current_user.gender, :partner_id => nil).where.not(id: @current_user.id).count
      random_animal = Baby.where(:gender => @current_user.gender, :partner_id => nil).where.not(id: @current_user.id).first(:offset => rand(count))
      if !random_animal.blank?
        @current_user.partner_id = random_animal.id
        random_animal.partner_id = @current_user.id

        team_id = Baby.maximum(:team_id).to_i
        if team_id.blank?
          team_id = 0
        end
        team_id += 1

        random_animal.team_id = team_id
        @current_user.team_id = team_id

        @current_user.is_selector = true

        random_animal.save()
        @current_user.save()

        response = {status: 'SUCCESS'}
      else
        response = {status: 'ERROR', message: 'No animal available to be your partner. Enjoy your long lonely life!'}
      end
    end

    render :json => response, :layout => false
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def baby_params
      params[:baby]
    end
end
