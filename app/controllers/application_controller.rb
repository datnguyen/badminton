class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


  def accessible?
    @current_user = session[:current_user]
    if @current_user.blank?
      redirect_to :controller => :babies, :action => :wheres_your_key
    end
  end
end
