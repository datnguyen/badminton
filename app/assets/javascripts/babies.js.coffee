# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $("#select-partner").click (e) ->
    $self = $(this)

    $('#select-partner').text('We\'re hard working to find you a partner...').attr('disabled', 'disabled')

    setTimeout (->
      $.ajax
        url: $self.data('url')
        dataType: "json"
        type: "post"
        success: (data) ->
          if data.status is 'SUCCESS'
            #$('#select-partner').remove()
            window.location.reload()
          else
            $('#message').html(data.message)
    ), 2000
