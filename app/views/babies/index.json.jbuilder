json.array!(@babies) do |baby|
  json.extract! baby, :id
  json.url baby_url(baby, format: :json)
end
