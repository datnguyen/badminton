class CreateBabies < ActiveRecord::Migration
  def change
    create_table :babies do |t|

      t.timestamps
    end

    add_column :babies, :name, :string
    add_column :babies, :access_code, :string
    add_column :babies, :rate, :integer
    add_column :babies, :partner_id, :integer
  end
end
