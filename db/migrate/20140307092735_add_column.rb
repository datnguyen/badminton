class AddColumn < ActiveRecord::Migration
  def change
    add_column :babies, :is_selector, :boolean
    add_column :babies, :team_id, :integer
    add_column :babies, :gender, :boolean
  end
end
